Receipt application
================================================

Goals
-----
- Develop a simple Spring Boot application with CRUD operations for Receipt entity according to requirements specified in take-home-project.pdf

Requirements
------------
- [Docker](https://docs.docker.com/install/)
- [Docker Compose](https://docs.docker.com/compose/install/)

Development
-----------
1. Run Postgres database as Docker container:
    - Go to `docker` location
    - Go to location of docker-compose.yml: `docker-compose up -d postgres`
2. Go to location of `pom.xml` and run Spring Boot application: `mvn spring-boot:run`

Docker Compose usage
--------------------
1. Go to `docker` location
2. Run Bash script to build Docker-Compose images: `./build-docker.sh`
3. Go to `docker/app` location (where `docker-compose.yml` is located)
4. Run Docker-Compose command to create and start application and database: `docker-compose up -d`
5. When application starts, test API: `curl http://localhost:8080/api/receipts/<receiptId>`
6. Run Docker-Compose command to stop and destroy application and database: `docker-compose down`

##### Docker Compose commands:
 - `docker-compose up` - create and start Docker containers
 - `docker-compose up -d` - create and start Docker cotainers in the background
 - `docker-compose down` - stop and destroy Docker cotainers
 - `docker-compose start` - start Docker containers
 - `docker-compose stop` - stop Docker containers
 - `docker-compose logs -f` - tailing logs of Docker cotainers
 - `docker-compose ps` - check status of Docker cotainers