package com.getupside.services;

import com.getupside.domain.entity.Receipt;
import com.getupside.repository.ReceiptRepository;
import com.getupside.web.converter.ReceiptDtoToReceiptConverter;
import com.getupside.web.converter.ReceiptToReceiptDtoConverter;
import com.getupside.web.dto.ReceiptDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class ReceiptService {

    private final ReceiptRepository receiptRepository;
    private final ReceiptDtoToReceiptConverter receiptDtoToReceiptConverter;
    private final ReceiptToReceiptDtoConverter receiptToReceiptDtoConverter;

    @Autowired
    public ReceiptService(ReceiptRepository receiptRepository,
                          ReceiptDtoToReceiptConverter receiptDtoToReceiptConverter,
                          ReceiptToReceiptDtoConverter receiptToReceiptDtoConverter) {
        this.receiptRepository = receiptRepository;
        this.receiptDtoToReceiptConverter = receiptDtoToReceiptConverter;
        this.receiptToReceiptDtoConverter = receiptToReceiptDtoConverter;
    }

    @Transactional
    public ReceiptDto createReceipt(ReceiptDto receiptDto) {
        receiptDto.setId(null);
        Receipt receipt = receiptDtoToReceiptConverter.convert(receiptDto);
        Receipt receiptCreated = receiptRepository.save(receipt);
        return receiptToReceiptDtoConverter.convert(receiptCreated);
    }

    @Transactional
    public void updateReceipt(Long receiptId, ReceiptDto receiptDto) {
        Receipt receipt = receiptRepository.findById(receiptId)
                .orElseThrow(ResourceNotFoundException::new);
        receiptDtoToReceiptConverter.convert(receiptDto, receipt);
        receiptRepository.save(receipt);
    }

    @Transactional
    public void deleteReceipt(Long receiptId) {
        receiptRepository.deleteById(receiptId);
    }

    public ReceiptDto getReceipt(Long receiptId) {
        return receiptRepository.findById(receiptId)
                .map(receiptToReceiptDtoConverter::convert)
                .orElseThrow(ResourceNotFoundException::new);
    }
}
