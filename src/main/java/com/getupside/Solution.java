package com.getupside;


import java.util.ArrayList;
import java.util.List;

public class Solution {

    public static void main(String[] args) {
        Solution solution = new Solution();
        Node node = new Node();
        node.val = 2;
        node.neighbors = new ArrayList<>();
        solution.cloneGraph(node);
    }

    public Node cloneGraph(Node node) {
        if (node == null) {
            return null;
        }
        Node[] visited = new Node[101];
        return cloneNeighbors(node, visited);
    }

    private Node cloneNeighbors(Node node, Node[] visited) {
        if (node == null) {
            return null;
        }
        if (visited[node.val] != null) {
            return visited[node.val];
        }
        Node clonedNode = new Node(node.val);
        visited[node.val] = clonedNode;
        for (Node neighbor : clonedNode.neighbors) {
            clonedNode.neighbors.add(cloneNeighbors(neighbor, visited));
        }
        return clonedNode;
    }
}

class Node {
    public int val;
    public List<Node> neighbors;

    public Node() {
        val = 0;
        neighbors = new ArrayList<Node>();
    }

    public Node(int _val) {
        val = _val;
        neighbors = new ArrayList<Node>();
    }

    public Node(int _val, ArrayList<Node> _neighbors) {
        val = _val;
        neighbors = _neighbors;
    }
}