package com.getupside.web.converter;

import com.getupside.domain.entity.Receipt;
import com.getupside.web.dto.ReceiptDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ReceiptToReceiptDtoConverter implements Converter<Receipt, ReceiptDto> {

    private final ModelMapper modelMapper;

    @Autowired
    public ReceiptToReceiptDtoConverter(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public ReceiptDto convert(Receipt receipt) {
        return modelMapper.map(receipt, ReceiptDto.class);
    }
}
