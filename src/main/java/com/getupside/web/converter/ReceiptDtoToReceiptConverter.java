package com.getupside.web.converter;

import com.getupside.domain.entity.Receipt;
import com.getupside.web.dto.ReceiptDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ReceiptDtoToReceiptConverter {

    private final ModelMapper modelMapper;

    @Autowired
    public ReceiptDtoToReceiptConverter(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public Receipt convert(ReceiptDto receiptDto) {
        return modelMapper.map(receiptDto, Receipt.class);
    }

    public void convert(ReceiptDto receiptDto, Receipt receipt) {
        modelMapper.map(receiptDto, receipt);
    }
}
