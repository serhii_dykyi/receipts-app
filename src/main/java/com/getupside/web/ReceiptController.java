package com.getupside.web;

import com.getupside.services.ReceiptService;
import com.getupside.web.dto.ReceiptDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/receipts")
public class ReceiptController {

    private final ReceiptService receiptService;

    @Autowired
    public ReceiptController(ReceiptService receiptService) {
        this.receiptService = receiptService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ReceiptDto createReceipt(@RequestBody ReceiptDto receiptDto) {
        return receiptService.createReceipt(receiptDto);
    }

    @PutMapping(value = "/{receiptId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateReceipt(@PathVariable Long receiptId, @RequestBody ReceiptDto receiptDto) {
        receiptService.updateReceipt(receiptId, receiptDto);
    }

    @DeleteMapping(value = "/{receiptId}")
    public void deleteReceipt(@PathVariable Long receiptId) {
        receiptService.deleteReceipt(receiptId);
    }

    @GetMapping(value = "/{receiptId}")
    public ReceiptDto getReceipt(@PathVariable Long receiptId) {
        return receiptService.getReceipt(receiptId);
    }
}
