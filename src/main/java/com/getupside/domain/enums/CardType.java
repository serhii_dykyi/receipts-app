package com.getupside.domain.enums;

public enum CardType {

    VISA, MASTERCARD, MAESTRO;
}
