CREATE TABLE receipt (
	id int8 NOT NULL GENERATED BY DEFAULT AS IDENTITY,
	amount numeric(19,2) NULL,
	card_type varchar(255) NULL,
	credit_card varchar(255) NULL,
	"date" date NULL,
	site_uuid uuid NULL,
	source_terminal varchar(255) NULL,
	tax_amount numeric(19,2) NULL,
	"time" time NULL,
	uuid uuid NULL,
	CONSTRAINT receipt_pkey PRIMARY KEY (id)
);
