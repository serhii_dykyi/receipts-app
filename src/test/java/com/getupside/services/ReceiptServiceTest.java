package com.getupside.services;

import com.getupside.domain.entity.Receipt;
import com.getupside.repository.ReceiptRepository;
import com.getupside.web.converter.ReceiptDtoToReceiptConverter;
import com.getupside.web.converter.ReceiptToReceiptDtoConverter;
import com.getupside.web.dto.ReceiptDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;

import java.util.Optional;

import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ReceiptServiceTest {

    private static final Long RECEIPT_ID = 123L;
    private static final ReceiptDto INPUT_RECEIPT = new ReceiptDto();
    private static final ReceiptDto OUTPUT_RECEIPT = new ReceiptDto();

    private ReceiptRepository receiptRepository = mock(ReceiptRepository.class);
    private ReceiptDtoToReceiptConverter receiptDtoToReceiptConverter = mock(ReceiptDtoToReceiptConverter.class);
    private ReceiptToReceiptDtoConverter receiptToReceiptDtoConverter = mock(ReceiptToReceiptDtoConverter.class);

    @InjectMocks
    private ReceiptService receiptService = new ReceiptService(
            receiptRepository,
            receiptDtoToReceiptConverter,
            receiptToReceiptDtoConverter);

    @Test
    public void shouldCreateReceipt() {
        //given
        Receipt receipt = new Receipt();
        Receipt receiptCreated = new Receipt();
        when(receiptDtoToReceiptConverter.convert(INPUT_RECEIPT)).thenReturn(receipt);
        when(receiptRepository.save(receipt)).thenReturn(receiptCreated);
        when(receiptToReceiptDtoConverter.convert(receiptCreated)).thenReturn(OUTPUT_RECEIPT);
        //when
        ReceiptDto receiptDto = receiptService.createReceipt(INPUT_RECEIPT);
        //then
        assertSame(receiptDto, OUTPUT_RECEIPT);
        verify(receiptRepository).save(receipt);
        verify(receiptToReceiptDtoConverter).convert(receiptCreated);
    }

    @Test
    public void shouldUpdateReceipt() {
        //given
        Receipt receipt = new Receipt();
        when(receiptRepository.findById(RECEIPT_ID)).thenReturn(Optional.of(receipt));
        //when
        receiptService.updateReceipt(RECEIPT_ID, INPUT_RECEIPT);
        //then
        verify(receiptRepository).findById(RECEIPT_ID);
        verify(receiptDtoToReceiptConverter).convert(INPUT_RECEIPT, receipt);
        verify(receiptRepository).save(receipt);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void shouldThrowResourceNotFoundExceptionWhenNoReceiptFoundToUpdate() {
        //given
        when(receiptRepository.findById(RECEIPT_ID)).thenReturn(Optional.empty());
        //when
        receiptService.updateReceipt(RECEIPT_ID, INPUT_RECEIPT);
    }

    @Test
    public void shouldDeleteReceipt() {
        //when
        receiptService.deleteReceipt(RECEIPT_ID);
        //then
        verify(receiptRepository).deleteById(RECEIPT_ID);
    }

    @Test
    public void shouldReturnReceipt() {
        //given
        Receipt receipt = new Receipt();
        when(receiptRepository.findById(RECEIPT_ID)).thenReturn(Optional.of(receipt));
        when(receiptToReceiptDtoConverter.convert(receipt)).thenReturn(OUTPUT_RECEIPT);
        //when
        ReceiptDto foundReceipt = receiptService.getReceipt(RECEIPT_ID);
        //then
        assertSame(foundReceipt, OUTPUT_RECEIPT);
        verify(receiptRepository).findById(RECEIPT_ID);
        verify(receiptToReceiptDtoConverter).convert(receipt);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void shouldThrowResourceNotFoundExceptionWhenNoReceiptFound() {
        //given
        when(receiptRepository.findById(RECEIPT_ID)).thenReturn(Optional.empty());
        //when
        receiptService.getReceipt(RECEIPT_ID);
    }
}
