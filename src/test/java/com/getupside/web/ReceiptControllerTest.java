package com.getupside.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.getupside.domain.enums.CardType;
import com.getupside.services.ReceiptService;
import com.getupside.web.dto.ReceiptDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ReceiptController.class)
public class ReceiptControllerTest {

    @Autowired
    private MockMvc mvc;
    @MockBean
    private ReceiptService receiptService;
    @SpyBean
    private ModelMapper modelMapper;

    @Test
    public void shouldReturnReceipt() throws Exception {
        ReceiptDto receiptDto = new ReceiptDto();
        receiptDto.setCardType(CardType.VISA.name());

        when(receiptService.getReceipt(123L)).thenReturn(receiptDto);

        mvc.perform(get("/receipts/123")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.cardType", is(CardType.VISA.name())));

        verify(receiptService).getReceipt(123L);
    }

    @Test
    public void shouldReturnNotFoundResponseWhenNoReceiptFoundById() throws Exception {
        when(receiptService.getReceipt(123L)).thenThrow(ResourceNotFoundException.class);

        mvc.perform(get("/receipts/123")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$").doesNotExist());

        verify(receiptService).getReceipt(123L);
    }

    @Test
    public void shouldDeleteReceipt() throws Exception {
        mvc.perform(delete("/receipts/123")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").doesNotExist());

        verify(receiptService).deleteReceipt(123L);
    }

    @Test
    public void shouldCreateReceipt() throws Exception {
        ReceiptDto receiptDto = new ReceiptDto();
        receiptDto.setCardType(CardType.VISA.name());

        when(receiptService.createReceipt(ArgumentMatchers.any(ReceiptDto.class))).thenReturn(receiptDto);

        mvc.perform(post("/receipts")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(receiptDto)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$").exists());

        verify(receiptService).createReceipt(ArgumentMatchers.any(ReceiptDto.class));
    }

    @Test
    public void shouldUpdateReceipt() throws Exception {
        ReceiptDto receiptDto = new ReceiptDto();
        receiptDto.setCardType(CardType.VISA.name());

        mvc.perform(put("/receipts/123")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(receiptDto)))
                .andExpect(status().isNoContent())
                .andExpect(jsonPath("$").doesNotExist());

        verify(receiptService).updateReceipt(eq(123L), ArgumentMatchers.any(ReceiptDto.class));
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}