package com.getupside.web.converter;

import com.getupside.domain.entity.Receipt;
import com.getupside.web.dto.ReceiptDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ReceiptDtoToReceiptConverterTest {

    private ModelMapper modelMapper = spy(ModelMapper.class);

    private ReceiptDtoToReceiptConverter converter = new ReceiptDtoToReceiptConverter(modelMapper);

    @Test
    public void shouldConvertReceiptDtoToReceipt() {
        //given
        ReceiptDto receiptDto = new ReceiptDto();
        //when
        converter.convert(receiptDto);
        //then
        verify(modelMapper).map(receiptDto, Receipt.class);
    }

    @Test
    public void shouldConvertReceiptDtoToReceiptWithPassedInEntity() {
        //given
        Receipt receipt = new Receipt();
        ReceiptDto receiptDto = new ReceiptDto();
        //when
        converter.convert(receiptDto, receipt);
        //then
        verify(modelMapper).map(receiptDto, receipt);
    }
}