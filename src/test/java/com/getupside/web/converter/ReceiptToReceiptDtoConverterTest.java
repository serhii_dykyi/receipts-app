package com.getupside.web.converter;

import com.getupside.domain.entity.Receipt;
import com.getupside.domain.enums.CardType;
import com.getupside.web.dto.ReceiptDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ReceiptToReceiptDtoConverterTest {

    private static final BigDecimal AMOUNT = new BigDecimal(1.23);
    private static final CardType CARD_TYPE = CardType.VISA;
    private static final String CREDIT_CARD = "123456XXXXXX9876";
    private static final LocalDate DATE = LocalDate.parse("2019-01-11");
    private static final LocalTime TIME = LocalTime.parse("15:16:45");
    private static final UUID SITE_UUID = UUID.fromString("28e6abd2-df96-4e07-b2bc-bc82fef35a55");
    private static final String SOURCE_TERMINAL = "OUTSIDE";
    private static final BigDecimal TAX_AMOUNT = BigDecimal.ZERO;
    private static final UUID UUID_VALUE = UUID.fromString("70f40fd4-fb67-4566-9e42-361e2cd241b6");

    private ModelMapper modelMapper = spy(ModelMapper.class);

    private ReceiptToReceiptDtoConverter converter = new ReceiptToReceiptDtoConverter(modelMapper);

    @Test
    public void shouldConvertReceiptToReceiptDto() {
        //given
        Receipt receipt = createReceipt();
        //when
        ReceiptDto receiptDto = converter.convert(receipt);
        //then
        verify(modelMapper).map(receipt, ReceiptDto.class);

        assertNotNull(receiptDto);
        assertEquals(AMOUNT, receiptDto.getAmount());
        assertEquals(CARD_TYPE.name(), receiptDto.getCardType());
        assertEquals(CREDIT_CARD, receiptDto.getCreditCard());
        assertEquals(DATE, receiptDto.getDate());
        assertEquals(TIME, receiptDto.getTime());
        assertEquals(SITE_UUID, receiptDto.getSiteUuid());
        assertEquals(SOURCE_TERMINAL, receiptDto.getSourceTerminal());
        assertEquals(TAX_AMOUNT, receiptDto.getTaxAmount());
        assertEquals(UUID_VALUE, receiptDto.getUuid());
    }

    private Receipt createReceipt() {
        Receipt receipt = new Receipt();
        receipt.setAmount(AMOUNT);
        receipt.setCardType(CARD_TYPE);
        receipt.setCreditCard(CREDIT_CARD);
        receipt.setDate(DATE);
        receipt.setTime(TIME);
        receipt.setSiteUuid(SITE_UUID);
        receipt.setSourceTerminal(SOURCE_TERMINAL);
        receipt.setTaxAmount(TAX_AMOUNT);
        receipt.setUuid(UUID_VALUE);
        return receipt;
    }
}